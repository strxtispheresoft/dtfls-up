source $HOME/.config/nvim/vim-plug/plugins.vim

let mapleader=" "

set hidden

set nobackup
set noswapfile
set undofile

set noshowmode
set laststatus=2
set nohlsearch
set noerrorbells
set nowrap
set incsearch
set scrolloff=8
set signcolumn=yes

set termguicolors
set t_Co=256

let g:lightline = {
      \ 'colorscheme': 'onedark',
      \ }

syntax on
colorscheme onedark

set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent

set relativenumber
set nu

set updatetime=50

fun! TrimWhitespace()
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
    call winrestview(l:save)
endfun

augroup NATIVE
    autocmd!
    autocmd BufWritePre * :call TrimWhitespace()
augroup END

nnoremap <C-p> :Files<Cr>

nnoremap <leader>ff <cmd>lua require('telescope.builtin').find_files()<cr>
nnoremap <leader>fg <cmd>lua require('telescope.builtin').live_grep()<cr>
nnoremap <leader>fb <cmd>lua require('telescope.builtin').buffers()<cr>
nnoremap <leader>fh <cmd>lua require('telescope.builtin').help_tags()<cr>


