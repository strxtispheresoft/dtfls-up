#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# alias ls='ls --color=auto'
# PS1='[\u@\h \W]\$ '



# -- Aliases --

# - Soft Aliases -
alias v='nvim'
alias sp='spt'

# - Dir Aliases -
alias ..='cd ..'
alias sc='cd ~/dox/scrcp && ls'

# - Git Aliases -
alias gclone='git clone'
alias gpush='git push -u origin main'
alias gcommit='git commit -m'
alias gaa='git add .'
alias ginit='git init --initial-branch=main'
alias gra='git remote add origin'
alias grr='git remote rename origin old-origin'

# - File Aliases -
alias bashrc='v ~/.bashrc'
alias xmonad='v ~/.xmonad/xmonad.hs'
alias init='v ~/.config/x11/xinitrc'
alias alac='v ~/.config/alacritty/alacritty.yml'

# - Rand Aliases -

# - Pac + Paru Aliases -
alias allup='p -Syu && paru -Syu --noconfirm'
alias ccache='p -Sc --noconfirm'
alias rmir='p -Syyu && paru -Syyu --noconfirm'

# - Audio Aliases -

# - System Aliases -
alias ls='ls -la --color=auto'
alias c='clear'
alias p='sudo pacman'
alias s='sudo'

